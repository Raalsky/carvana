import cv2
import numpy as np
import pandas as pd
from os import listdir, makedirs
from os.path import isfile, join, exists
from tqdm import tqdm, tqdm_notebook
from skimage.io import imsave, imread
from pprint import pprint

TRAIN_PATH       = join('data', 'raw', 'train')
TRAIN_IMGS_PATH  = join(TRAIN_PATH, 'imgs')
TRAIN_MASKS_PATH = join(TRAIN_PATH, 'masks')
TEST_PATH        = join('data', 'raw', 'test')
TEST_IMGS_PATH   = join(TEST_PATH, 'imgs')
IMG_EXT          = 'jpg'
MASK_EXT         = 'gif'
MASK_OUTPUT_EXT  = 'png'
PROCESSED_PATH   = join('data', 'processed')

image_rows = 1280
image_cols = 1918

dest_rows = 512
dest_cols = 512

def read_resize(path, dest_rows, dest_cols, grayscale=False):
    __mode = 'RGB'
    if grayscale:
        __mode = 'L'
    return np.resize(
                np.array([
                    cv2.resize(
                        imread(path, mode=__mode),
                        None,
                        fx=dest_cols/image_cols,
                        fy=dest_rows/image_rows,
                        interpolation = cv2.INTER_AREA)
                ]),
                (dest_rows, dest_cols, 1))

def save_data(path, data):
    print('Saving %s' % path)
    np.save(path, data)

def create_train_data():
    filenames = {f[:-4] : (join(TRAIN_IMGS_PATH, f), join(TRAIN_MASKS_PATH, '%s_mask.gif' % f[:-4])) for f in listdir(TRAIN_IMGS_PATH) if f.endswith("jpg")}

    RESOLUTION_PATH = join(PROCESSED_PATH, '%dx%d' % (dest_cols, dest_rows))
    TRAIN_PROCESSED_PATH = join(RESOLUTION_PATH, 'train')

    if not exists(TRAIN_PROCESSED_PATH):
        makedirs(TRAIN_PROCESSED_PATH)

    total = len(filenames.values())

    imgs = np.ndarray((total, dest_rows, dest_cols, 3), dtype=np.uint8)
    masks = np.ndarray((total, dest_rows, dest_cols, 1), dtype=np.uint8)

    i = 0
    for name, (img_path, mask_path) in tqdm(filenames.items()):
        imgs[i] = read_resize(img_path, dest_rows, dest_cols)
        masks[i] = read_resize(mask_path, dest_rows, dest_cols, grayscale=True)

        i += 1

    save_data(join(TRAIN_PROCESSED_PATH, 'imgs.npy'), imgs)
    save_data(join(TRAIN_PROCESSED_PATH, 'masks.npy'), masks)

def load_train_data():
    RESOLUTION_PATH = join(PROCESSED_PATH, '%dx%d' % (dest_cols, dest_rows))
    TRAIN_PROCESSED_PATH = join(RESOLUTION_PATH, 'train')

    imgs_train = np.load(join(TRAIN_PROCESSED_PATH, 'imgs.npy'))
    masks_train = np.load(join(TRAIN_PROCESSED_PATH, 'masks.npy'))
    return imgs_train, masks_train

def get_chunks(list, n=1000):
    for i in range(0, len(list), n):
        yield list[i:i + n]

def create_test_data():
    filenames = {f[:-4] : join(TEST_IMGS_PATH, f) for f in listdir(TEST_IMGS_PATH) if f.endswith("jpg")}

    data_chunks = list(get_chunks(list(filenames.keys())))

    RESOLUTION_PATH = join(PROCESSED_PATH, '%dx%d' % (dest_cols, dest_rows))
    TEST_PROCESSED_PATH = join(RESOLUTION_PATH, 'test')

    if not exists(TEST_PROCESSED_PATH):
        makedirs(TEST_PROCESSED_PATH)

    with tqdm(total=len(filenames.items())) as pbar:
        for chunk_id, chunk in enumerate(data_chunks):
            total = len(chunk)
            imgs = np.ndarray((total, dest_rows, dest_cols, 3), dtype=np.uint8)
            imgs_id = [None] * total

            i = 0
            for name in chunk:
                img_path = filenames[name]
                imgs[i]    = read_resize(img_path, dest_rows, dest_cols)
                imgs_id[i] = name
                pbar.update(1)
                i += 1

            np.savez(join(TEST_PROCESSED_PATH, 'test_%03d' % chunk_id), imgs=imgs, ids=imgs_id)
    pbar.close()

def chunks_count():
    RESOLUTION_PATH = join(PROCESSED_PATH, '%dx%d' % (dest_cols, dest_rows))
    TEST_PROCESSED_PATH = join(RESOLUTION_PATH, 'test')

    chunks = {f[:-4] : join(TEST_PROCESSED_PATH, f) for f in listdir(TEST_PROCESSED_PATH) if f.endswith("npz")}

    return len(chunks.keys())

def load_test_chunks():
    RESOLUTION_PATH = join(PROCESSED_PATH, '%dx%d' % (dest_cols, dest_rows))
    TEST_PROCESSED_PATH = join(RESOLUTION_PATH, 'test')

    chunks = {f[:-4] : join(TEST_PROCESSED_PATH, f) for f in listdir(TEST_PROCESSED_PATH) if f.endswith("npz")}

    for chunk_name, chunk_path in chunks.items():
        data = np.load(chunk_path)
        yield data['imgs'], data['ids']

if __name__ == '__main__':
    create_train_data()
    create_test_data()

    #for imgs, imgs_id in load_chunks():
    #    print(len(imgs_id))
